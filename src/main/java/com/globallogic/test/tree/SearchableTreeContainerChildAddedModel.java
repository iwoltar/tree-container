package com.globallogic.test.tree;

import com.globallogic.test.tree.event.TreeChildAddedEvent;
import com.globallogic.test.tree.event.listener.TreeChildAddedListener;
import com.globallogic.test.tree.event.model.ChildAddedTreeEventModel;

import java.util.ArrayList;
import java.util.List;

public class SearchableTreeContainerChildAddedModel<T> implements SearchableTreeNode<T>, TreeContainer<T>,
        ChildAddedTreeEventModel<T> {

    private TreeNode<T> root;
    private List<TreeChildAddedListener<T>> listeners;

    public SearchableTreeContainerChildAddedModel(){
    }

    public SearchableTreeContainerChildAddedModel(TreeNode<T> root){
        this.root = root;
    }

    public TreeNode<T> getRoot() {
        return root;
    }

    public void setRoot(TreeNode<T> root) {
        this.root = root;
    }

    public List<TreeChildAddedListener<T>> getListeners() {
        return listeners;
    }

    public void setListeners(List<TreeChildAddedListener<T>> listeners) {
        this.listeners = listeners;
    }

    @Override
    public int countNodes() {
        int nodesCount = 0;

        if(root != null) {
            nodesCount = countNestedNodes(root) + 1;
        }

        return nodesCount;
    }

    @Override
    public int countNestedNodes(TreeNode<T> node) {
        int nodesCount = node.getChildCount();

        for(TreeNode<T> child : node.getChildren()) {
            nodesCount += countNestedNodes(child);
        }

        return nodesCount;
    }

    @Override
    public boolean contains(T data) {
        return find(data) != null;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public TreeNode<T> find(T data) {
        TreeNode<T> foundNode = null;

        if(root != null) {
            foundNode = nestedFind(root, data);
        }

        return foundNode;
    }

    @Override
    public TreeNode<T> nestedFind(TreeNode<T> treeNode, T data) {
        TreeNode<T> foundNode = null;

        if (treeNode.getData() != null && treeNode.getData().equals(data)) {
            foundNode = treeNode;
        } else if(treeNode.hasChildren()) {
            int childIndex = 0;
            while(foundNode == null && childIndex < treeNode.getChildCount()) {
                foundNode = nestedFind(treeNode.getChildAt(childIndex), data);
                childIndex++;
            }
        }

        return foundNode;
    }

    @Override
    public TreeNode<T> findByCriteria(Predicate<T> criteria) {
        TreeNode<T> foundNode = null;

        if(root != null) {
            foundNode = nestedFindByCriteria(root, criteria);
        }

        return foundNode;
    }

    @Override
    public TreeNode<T> nestedFindByCriteria(TreeNode<T> treeNode, Predicate<T> criteria) {
        TreeNode<T> foundNode = null;

        if (treeNode.getData() != null && criteria.apply(treeNode.getData())) {
            foundNode = treeNode;
        } else if(treeNode.hasChildren()) {
            int childIndex = 0;
            while(foundNode == null && childIndex < treeNode.getChildCount()) {
                foundNode = nestedFindByCriteria(treeNode.getChildAt(childIndex), criteria);
                childIndex++;
            }
        }

        return foundNode;
    }

    @Override
    public TreeNode<T> addChild(TreeNode<T> child) {
        child = this.root.add(child);
        notifyChildAddedListeners(child);
        return child;
    }

    @Override
    public void addChildAddedListener(TreeChildAddedListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException("Argument is null");
        }

        if (listeners == null) {
            listeners = new ArrayList<>();
        }

        listeners.add(listener);
    }

    @Override
    public void removeChildAddedListener(TreeChildAddedListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException("Argument is null");
        }

        if (listeners != null) {
            listeners.remove(listener);
        }
    }

    public void notifyChildAddedListeners (TreeNode<T> child) {
        this.listeners.forEach(listener -> listener.onChildAdded(
                new TreeChildAddedEvent<T>(root, child, root.getIndex(child))));
    }
}
