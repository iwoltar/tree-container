package com.globallogic.test.tree.event.model;

import com.globallogic.test.tree.TreeNode;
import com.globallogic.test.tree.event.listener.TreeChildAddedListener;

public interface ChildAddedTreeEventModel<T> {

    TreeNode<T> addChild(TreeNode<T> child);

    void addChildAddedListener(TreeChildAddedListener listener);

    void removeChildAddedListener(TreeChildAddedListener listener);
}