package com.globallogic.test.tree.event.listener;

import com.globallogic.test.tree.event.TreeChildAddedEvent;

public class PrintDetailsTreeChildAddedListener<T> implements TreeChildAddedListener<T> {

    @Override
    public void onChildAdded(TreeChildAddedEvent<T> e) {
        log("Added a new child: " + e.getChild().toString() +
                "\nindex: " + e.getChildIndex() +
                "\nspot left: " + (e.getSource().getMaxChildCount() - e.getSource().getChildCount()));
    }

    private void log(String text) {
        System.out.println(text);
    }
}