package com.globallogic.test.tree.event.listener;

import com.globallogic.test.tree.event.TreeChildAddedEvent;
import com.globallogic.test.tree.event.listener.EventListener;

public interface TreeChildAddedListener<T> extends EventListener<T> {

    void onChildAdded(TreeChildAddedEvent<T> e);
}