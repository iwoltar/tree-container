package com.globallogic.test.tree.event;

import com.globallogic.test.tree.TreeNode;

public class TreeEvent<T> {

    private TreeNode<T> source;

    public TreeEvent(TreeNode<T> source) {
        if (source == null)
            throw new IllegalArgumentException("Null source");

        this.source = source;
    }

    public TreeNode<T> getSource() {
        return source;
    }

    public String toString() {
        return getClass().getName() + "[source=" + source.getData() + "]";
    }
}
