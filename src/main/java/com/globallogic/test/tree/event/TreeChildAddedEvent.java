package com.globallogic.test.tree.event;

import com.globallogic.test.tree.TreeNode;

public class TreeChildAddedEvent<T> extends TreeEvent<T> {
    private int childIndex;
    private TreeNode<T> child;

    public TreeChildAddedEvent(TreeNode<T> source, TreeNode<T> child, int childIndex) {
        super(source);
        this.child = child;
        this.childIndex = childIndex;
    }

    public int getChildIndex() {
        return childIndex;
    }

    public TreeNode<T> getChild() {
        return child;
    }
}
