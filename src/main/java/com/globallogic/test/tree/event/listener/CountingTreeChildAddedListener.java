package com.globallogic.test.tree.event.listener;

import com.globallogic.test.tree.event.TreeChildAddedEvent;

public class CountingTreeChildAddedListener implements TreeChildAddedListener {

    private int childAddedCount = 0;

    @Override
    public void onChildAdded(TreeChildAddedEvent e) {
        childAddedCount++;

        System.out.println("Total children added: " + childAddedCount);
    }
}