package com.globallogic.test.tree;

import java.util.ArrayList;
import java.util.List;

public class TreeNode<T> {

    private T data;

    private TreeNode<T> parent;

    private List<TreeNode<T>> children;

    private int maxChildCount = 1 << 10; // aka 1024

    public TreeNode() {
        this.children = new ArrayList<TreeNode<T>>();
    }

    public TreeNode(T data) {
        this.data = data;
        this.children = new ArrayList<TreeNode<T>>();
    }

    public TreeNode(T data, int maxChildCount) {
        this.data = data;
        this.maxChildCount = maxChildCount;
        this.children = new ArrayList<TreeNode<T>>();
    }

    public TreeNode<T> getParent() {
        return parent;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<TreeNode<T>> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode<T>> children) {
        children.forEach(child -> {
            child.parent = this;
            child.setMaxChildCount(maxChildCount);
        });

        this.children = children;
    }

    public boolean hasChildren() {
        return children.size() > 0;
    }

    public int getIndex(TreeNode<T> child) {
        if (child == null) {
            throw new IllegalArgumentException("Argument is null");
        }

        if (!isChild(child)) {
            return -1;
        }
        return this.children.indexOf(child);
    }

    public TreeNode<T> getChildAt(int index) {
        if (this.children == null || this.children.isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Container has no children");
        }
        return this.children.get(index);
    }

    public TreeNode<T> add(TreeNode<T> child) {
        return insert(child, getChildCount());
    }

    public TreeNode<T> insert(TreeNode<T> child, int index) {
        if (child == null) {
            throw new IllegalArgumentException("New child is null");
        } else if (maxChildCount <= getChildCount()) {
            throw new IllegalStateException("Container is full");
        } else if (isContainerAncestor(child)) {
            throw new IllegalArgumentException("New child is an ancestor");
        }

        TreeNode<T> oldParent = child.getParent();

        if (oldParent != null) {
            oldParent.remove(child);
        }

        child.parent = this;
        child.maxChildCount = maxChildCount;

        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(index, child);

        return child;
    }

    public boolean remove(TreeNode<T> child) {
        if (child == null) {
            throw new IllegalArgumentException("Argument is null");
        }

        if (!isChild(child)) {
            throw new IllegalArgumentException("Argument is not a child");
        }

        return remove(getIndex(child));
    }

    public boolean remove(int childIndex) {
        TreeNode<T> child = getChildAt(childIndex);
        TreeNode<T> removed = children.remove(childIndex);
        child.parent = null;
        return removed != null;
    }

    public boolean isContainerAncestor(TreeNode<T> container) {
        if (container == null) {
            return false;
        }

        TreeNode<T> ancestor = this;

        do {
            if (ancestor == container) {
                return true;
            }
        } while((ancestor = ancestor.getParent()) != null);

        return false;
    }

    public boolean isChild(TreeNode<T> child) {
        boolean isChild;

        if (child == null) {
            isChild = false;
        } else {
            if (getChildCount() == 0) {
                isChild = false;
            } else {
                isChild = child.getParent() == this;
            }
        }

        return isChild;
    }

    public int getChildCount() {
        return children.size();
    }

    public int getMaxChildCount() {
        return maxChildCount;
    }

    public void setMaxChildCount(int maxChildCount) {
        this.maxChildCount = maxChildCount;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TreeNode<?> other = (TreeNode<?>) obj;
        if (data == null) {
            return other.data == null;
        } else {
            return data.equals(other.data);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((data == null) ? 0 : data.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return data != null ? "[data " +  data.toString() + "]" : "[data null]";
    }
}
