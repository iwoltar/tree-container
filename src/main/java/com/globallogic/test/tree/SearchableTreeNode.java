package com.globallogic.test.tree;

public interface SearchableTreeNode<T> {

    TreeNode<T> find(T data);

    TreeNode<T> nestedFind(TreeNode<T> treeNode, T data);

    TreeNode<T> findByCriteria(Predicate<T> criteria);

    TreeNode<T> nestedFindByCriteria(TreeNode<T> treeNode, Predicate<T> criteria);
}
