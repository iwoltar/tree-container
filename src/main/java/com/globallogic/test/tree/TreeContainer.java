package com.globallogic.test.tree;

public interface TreeContainer<T> {

    TreeNode<T> getRoot();

    void setRoot(TreeNode<T> root);

    int countNodes();

    int countNestedNodes(TreeNode<T> node);

    boolean contains(T data);

    boolean isEmpty();
}
