package com.globallogic.test.tree;

public interface Predicate<T> {

    boolean apply(T o);
}
