package com.globallogic.test.tree

import com.globallogic.test.tree.event.listener.PrintDetailsTreeChildAddedListener
import com.globallogic.test.tree.event.listener.TreeChildAddedListener
import spock.lang.Specification

class SearchableTreeContainerChildAddedModelSpec extends Specification {

    Predicate<String> testSearchCriteria = new Predicate<String>() {
        @Override
        boolean apply(String data) {
            return data.contains("child")
        }
    }

    def "TreeContainer root is null after default creation"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer
        when:
        treeContainer = new SearchableTreeContainerChildAddedModel<>()
        then:
        treeContainer.getRoot() == null
    }

    def "DefaultChildAddedTreeEventModel listeners is null after creation"() {
        given:
        SearchableTreeContainerChildAddedModel<String> eventModel
        when:
        eventModel = new SearchableTreeContainerChildAddedModel<>(new TreeNode<String>())
        then:
        eventModel.getListeners() == null
    }

    def "DefaultChildAddedTreeEventModel sets and gets root"() {
        given:
        SearchableTreeContainerChildAddedModel<String> eventModel =
                new SearchableTreeContainerChildAddedModel<>(new TreeNode<String>())
        TreeNode<String> newRoot = new TreeNode<String>()
        when:
        eventModel.setRoot(newRoot)
        then:
        eventModel.getRoot() == newRoot
    }

    def "TreeContainer root is non null after parametrized creation"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer
        when:
        treeContainer = new SearchableTreeContainerChildAddedModel<>(new TreeNode<String>())
        then:
        treeContainer.getRoot() != null
    }

    def "TreeContainer countNodes returns zero after default creation"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer
        when:
        treeContainer = new SearchableTreeContainerChildAddedModel<>()
        then:
        treeContainer.countNodes() == 0
    }

    def "TreeContainer countNodes returns one after parametrized creation"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer
        when:
        treeContainer = new SearchableTreeContainerChildAddedModel<>(new TreeNode<String>())
        then:
        treeContainer.countNodes() == 1
    }

    def "TreeContainer isEmpty is true after default creation"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer
        when:
        treeContainer = new SearchableTreeContainerChildAddedModel<>()
        then:
        treeContainer.isEmpty()
    }

    def "TreeContainer isEmpty is false after parametrized creation"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer
        when:
        treeContainer = new SearchableTreeContainerChildAddedModel<>(new TreeNode<String>())
        then:
        !treeContainer.isEmpty()
    }

    def "TreeContainer contains is false after default creation"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer
        String data = "root"
        when:
        treeContainer = new SearchableTreeContainerChildAddedModel<>()
        then:
        !treeContainer.contains(data)
    }

    def "TreeContainer contains is true after parametrized creation"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer
        String data = "root"
        TreeNode<String> root = new TreeNode<String>(data)
        when:
        treeContainer = new SearchableTreeContainerChildAddedModel<>(root)
        then:
        treeContainer.contains(data)
    }

    def "TreeContainer contains is true for root child"() {
        given:
        String data = "root"
        TreeNode<String> root = new TreeNode<String>()
        root.add(new TreeNode<String>(data))
        SearchableTreeContainerChildAddedModel<String> treeContainer
        when:
        treeContainer = new SearchableTreeContainerChildAddedModel<>(root)
        then:
        treeContainer.contains(data)
    }

    def "TreeContainer find returns null after default creation"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer
        String data = "node"
        when:
        treeContainer = new SearchableTreeContainerChildAddedModel<>()
        then:
        treeContainer.find(data) == null
    }

    def "TreeContainer find returns child tree node"() {
        given:
        String data = "root"
        TreeNode<String> root = new TreeNode<String>()
        root.add(new TreeNode<String>(data))
        SearchableTreeContainerChildAddedModel<String> treeContainer = new SearchableTreeContainerChildAddedModel<>(root)
        when:
        TreeNode<String> result = treeContainer.find(data)
        then:
        result.getData() == data
    }

    def "TreeContainer nestedFind returns null after default creation"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer
        String data = "node"
        when:
        treeContainer = new SearchableTreeContainerChildAddedModel<>()
        then:
        treeContainer.nestedFind(new TreeNode<String>(), data) == null
    }

    def "TreeContainer nestedFind returns child tree node"() {
        given:
        String data = "child11"
        TreeNode<String> root = new TreeNode<String>()
        TreeNode<String> child = new TreeNode<String>("child1")
        child.add(new TreeNode<String>(data))
        root.add(child)
        SearchableTreeContainerChildAddedModel<String> treeContainer = new SearchableTreeContainerChildAddedModel<>(root)
        when:
        TreeNode<String> result = treeContainer.nestedFind(child, data)
        then:
        result.getData() == data
        result.getParent() == child
    }

    def "TreeContainer findByCriteria returns null after default creation"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer
        when:
        treeContainer = new SearchableTreeContainerChildAddedModel<>()
        then:
        treeContainer.findByCriteria(testSearchCriteria) == null
    }

    def "TreeContainer findByCriteria returns node"() {
        given:
        String data = "child11"
        TreeNode<String> root = new TreeNode<String>()
        TreeNode<String> child = new TreeNode<String>(data)
        root.add(child)
        SearchableTreeContainerChildAddedModel<String> treeContainer = new SearchableTreeContainerChildAddedModel<>(root)
        when:
        TreeNode<String> result = treeContainer.findByCriteria(testSearchCriteria)
        then:
        result.getData() == data
    }

    def "TreeContainer nestedFindByCriteria returns null after default creation"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer
        when:
        treeContainer = new SearchableTreeContainerChildAddedModel<>()
        then:
        treeContainer.nestedFindByCriteria(new TreeNode<String>(), testSearchCriteria) == null
    }

    def "TreeContainer nestedFindByCriteria returns child node"() {
        given:
        String data = "child11"
        TreeNode<String> root = new TreeNode<String>()
        TreeNode<String> child = new TreeNode<String>("ch")
        child.add(new TreeNode<String>(data))
        root.add(child)
        SearchableTreeContainerChildAddedModel<String> treeContainer = new SearchableTreeContainerChildAddedModel<>(root)
        when:
        TreeNode<String> result = treeContainer.nestedFindByCriteria(child, testSearchCriteria)
        then:
        result.getData() == data
        result.getParent() == child
    }

    def "TreeContainer sets and gets root node"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer = new SearchableTreeContainerChildAddedModel<>()
        when:
        treeContainer.setRoot(new TreeNode<String>())
        then:
        treeContainer.getRoot() != null
    }

    def "TreeContainer countNodes returns correct number of nodes"() {
        given:
        SearchableTreeContainerChildAddedModel<String> treeContainer = new SearchableTreeContainerChildAddedModel<>()
        when:
        treeContainer.setRoot(new TreeNode<String>("root"))
        then:
        treeContainer.countNodes() == 1
    }

    def "TreeContainer countNodes returns correct number of nodes including nested nodes"() {
        given:
        TreeNode<String> rootNode = new TreeNode<String>()
        rootNode.add(new TreeNode<String>("child"))
        SearchableTreeContainerChildAddedModel<String> treeContainer = new SearchableTreeContainerChildAddedModel<>()
        when:
        treeContainer.setRoot(rootNode)
        then:
        treeContainer.countNodes() == 2
    }

    def "TreeContainer isEmpty is false with non null root"() {
        given:
        TreeNode<String> rootNode = new TreeNode<String>()
        SearchableTreeContainerChildAddedModel<String> treeContainer = new SearchableTreeContainerChildAddedModel<>()
        when:
        treeContainer.setRoot(rootNode)
        then:
        !treeContainer.isEmpty()
    }

    def "DefaultChildAddedTreeEventModel adds listener"() {
        given:
        SearchableTreeContainerChildAddedModel<String> eventModel =
                new SearchableTreeContainerChildAddedModel<>(new TreeNode<String>("root"))
        TreeChildAddedListener listener = new PrintDetailsTreeChildAddedListener()
        when:
        eventModel.addChildAddedListener(listener)
        then:
        eventModel.getListeners() != null
        eventModel.getListeners().contains(listener)
    }

    def "DefaultChildAddedTreeEventModel throws exception after adding null listener"() {
        given:
        SearchableTreeContainerChildAddedModel<String> eventModel =
                new SearchableTreeContainerChildAddedModel<>(new TreeNode<String>("root"))
        TreeChildAddedListener listener
        when:
        eventModel.addChildAddedListener(listener)
        then:
        IllegalArgumentException ex = thrown()
        ex.message == 'Argument is null'
    }

    def "DefaultChildAddedTreeEventModel removes listener"() {
        given:
        SearchableTreeContainerChildAddedModel<String> eventModel =
                new SearchableTreeContainerChildAddedModel<>(new TreeNode<String>("root"))
        TreeChildAddedListener listener = new PrintDetailsTreeChildAddedListener()
        eventModel.addChildAddedListener(listener)
        when:
        eventModel.removeChildAddedListener(listener)
        then:
        eventModel.getListeners().isEmpty()
    }

    def "DefaultChildAddedTreeEventModel throws exception after removing null listener"() {
        given:
        SearchableTreeContainerChildAddedModel<String> eventModel =
                new SearchableTreeContainerChildAddedModel<>(new TreeNode<String>("root"))
        TreeChildAddedListener listener
        when:
        eventModel.removeChildAddedListener(listener)
        then:
        IllegalArgumentException ex = thrown()
        ex.message == 'Argument is null'
    }

    def "DefaultChildAddedTreeEventModel notifyChildAddedListeners notifies all listeners after adding new child"() {
        given:
        SearchableTreeContainerChildAddedModel<String> eventModel = Mock()
        TreeNode<String> child = new TreeNode<String>("child")
        when:
        eventModel.addChild(child)
        eventModel.notifyChildAddedListeners(child)
        then:
        1 * eventModel.addChild(child)
        then:
        1 * eventModel.notifyChildAddedListeners(child)
    }
}

