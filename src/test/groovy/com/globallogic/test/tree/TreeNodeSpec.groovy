package com.globallogic.test.tree

import spock.lang.Specification

class TreeNodeSpec extends Specification {

    def "TreeNode data is null after default creation"() {
        given:
        TreeNode<String> treeNode
        when:
        treeNode = new TreeNode<>()
        then:
        treeNode.getData() == null
    }

    def "TreeNode parent is null after default creation"() {
        given:
        TreeNode<String> treeNode
        when:
        treeNode = new TreeNode<>()
        then:
        treeNode.getParent() == null
    }

    def "TreeNode has not null children list after default creation"() {
        given:
        TreeNode<String> treeNode
        when:
        treeNode = new TreeNode<>()
        then:
        treeNode.getChildren() != null
    }

    def "TreeNode has zero children list after default creation"() {
        given:
        TreeNode<String> treeNode
        when:
        treeNode = new TreeNode<>()
        then:
        treeNode.getChildren().isEmpty()
    }

    def "TreeNode hasChildren returns false after default creation"() {
        given:
        TreeNode<String> treeNode
        when:
        treeNode = new TreeNode<>()
        then:
        !treeNode.hasChildren()
    }


    def "TreeNode data is not null after parametrized creation"() {
        given:
        TreeNode<String> treeNode
        when:
        treeNode = new TreeNode<>("node")
        then:
        treeNode.getData() != null
    }

    def "TreeNode parent is null after parametrized creation"() {
        given:
        TreeNode<String> treeNode
        when:
        treeNode = new TreeNode<>("node")
        then:
        treeNode.getParent() == null
    }

    def "TreeNode has not null children list after parametrized creation"() {
        given:
        TreeNode<String> treeNode
        when:
        treeNode = new TreeNode<>("node")
        then:
        treeNode.getChildren() != null
    }

    def "TreeNode has zero children list after parametrized creation"() {
        given:
        TreeNode<String> treeNode
        when:
        treeNode = new TreeNode<>("node")
        then:
        treeNode.getChildren().isEmpty()
    }

    def "TreeNode hasChildren returns false after parametrized creation"() {
        given:
        TreeNode<String> treeNode
        when:
        treeNode = new TreeNode<>("node")
        then:
        !treeNode.hasChildren()
    }

    def "TreeNode default max children count is 1024"() {
        given:
        TreeNode<String> treeNode
        when:
        treeNode = new TreeNode<>()
        then:
        treeNode.getMaxChildCount() == 1024
    }

    def "TreeNode getMaxChildCount returns specified value after parametrized creation"() {
        given:
        TreeNode<String> treeNode
        int maxChildrenCount = 10
        when:
        treeNode = new TreeNode<>("node", maxChildrenCount)
        then:
        treeNode.getMaxChildCount() == maxChildrenCount
    }

    def "TreeNode sets and gets data"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        String data = "node"
        when:
        treeNode.setData(data)
        then:
        treeNode.getData() == data
    }

    def "TreeNode sets and gets maxChildrenCount"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        int maxChildrenCount = 10
        when:
        treeNode.setMaxChildCount(maxChildrenCount)
        then:
        treeNode.getMaxChildCount() == maxChildrenCount
    }

    def "TreeNode sets and gets children"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        TreeNode<String> child = new TreeNode<>()
        List<TreeNode<String>> children = Arrays.asList(child)
        when:
        treeNode.setChildren(children)
        then:
        treeNode.getChildren() == children
    }

    def "TreeNode children has correct parent"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        TreeNode<String> childNode = new TreeNode<>()
        List<TreeNode<String>> children = Arrays.asList(childNode)
        when:
        treeNode.setChildren(children)
        then:
        treeNode.getChildren() == children
        treeNode.getChildren().any { it.getParent() == treeNode }
    }

    def "TreeNode children has parent max сhildren сount"() {
        given:
        int maxChildCount = 1
        TreeNode<String> treeNode = new TreeNode<>("root", maxChildCount)
        TreeNode<String> childNode = new TreeNode<>()
        List<TreeNode<String>> children = Arrays.asList(childNode)
        when:
        treeNode.setChildren(children)
        then:
        treeNode.getChildren().any { it.getMaxChildCount() == maxChildCount }
    }

    def "TreeNode getChildAt returns correct child"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        TreeNode<String> child = new TreeNode<>()
        List<TreeNode<String>> children = Arrays.asList(child)
        treeNode.setChildren(children)
        when:
        TreeNode<String> result = treeNode.getChildAt(0)
        then:
        result == child
    }

    def "TreeNode getChildAt throws exception if there are no children"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        List<TreeNode<String>> children = Collections.emptyList()
        treeNode.setChildren(children)
        when:
        TreeNode<String> result = treeNode.getChildAt(0)
        then:
        ArrayIndexOutOfBoundsException ex = thrown()
        ex.message == 'Container has no children'
        result == null
    }

    def "TreeNode isChild returns true after adding new child"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        TreeNode<String> child = treeNode.add(new TreeNode<>("child"))
        when:
        boolean result = treeNode.isChild(child)
        then:
        result
    }

    def "TreeNode isChild returns false for alien child"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        when:
        boolean result = treeNode.isChild(new TreeNode<>("child"))
        then:
        !result
    }

    def "TreeNode getIndex returns correct child index"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        treeNode.add(new TreeNode<>("added"))
        TreeNode<String> child = treeNode.insert(new TreeNode<>("inserted"), 0)
        when:
        int result = treeNode.getIndex(child)
        then:
        result == 0
    }

    def "TreeNode getIndex throws exception if child is null"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        when:
        treeNode.getIndex(null)
        then:
        IllegalArgumentException ex = thrown()
        ex.message == 'Argument is null'
    }

    def "TreeNode getChildCount return children count"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        TreeNode<String> childNode = new TreeNode<>()
        treeNode.setChildren(Arrays.asList(childNode))
        when:
        int childCount = treeNode.getChildCount()
        then:
        childCount == 1
    }

    def "TreeNode adds new child"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        TreeNode<String> childNode = new TreeNode<>()
        when:
        treeNode.add(childNode)
        then:
        !treeNode.getChildren().isEmpty()
        treeNode.getChildCount() == 1
        treeNode.getChildren().contains(childNode)
    }

    def "TreeNode added child has correct parent"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        TreeNode<String> childNode = new TreeNode<>()
        when:
        treeNode.add(childNode)
        then:
        treeNode.getChildren().any { it.getParent() == treeNode }
    }

    def "TreeNode added child has parent max сhildren сount"() {
        given:
        int maxChildCount = 1
        TreeNode<String> treeNode = new TreeNode<>("root", maxChildCount)
        TreeNode<String> childNode = new TreeNode<>()
        when:
        treeNode.add(childNode)
        then:
        treeNode.getChildren().any { it.getMaxChildCount() == maxChildCount }
    }

    def "TreeNode throws exception after adding null"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        TreeNode<String> childNode = null
        when:
        treeNode.add(childNode)
        then:
        IllegalArgumentException ex = thrown()
        ex.message == 'New child is null'
        treeNode.getChildren().isEmpty()
    }

    def "TreeNode throws exception when max count child is reached"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>("root", 1)
        TreeNode<String> childNode01 = new TreeNode<>("child01")
        TreeNode<String> childNode02 = new TreeNode<>("child02")
        when:
        treeNode.add(childNode01)
        treeNode.add(childNode02)
        then:
        IllegalStateException ex = thrown()
        ex.message == 'Container is full'
        treeNode.getMaxChildCount() == treeNode.getChildCount()
    }

    def "TreeNode throws exception after adding parent"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>("root", 1)
        TreeNode<String> childNode01 = new TreeNode<>("child01")
        TreeNode<String> childNode02 = new TreeNode<>("child02")
        when:
        treeNode.add(childNode01)
        treeNode.add(childNode02)
        then:
        IllegalStateException ex = thrown()
        ex.message == 'Container is full'
        treeNode.getMaxChildCount() == treeNode.getChildCount()
    }

    def "TreeNode insert new child at index"() {
        given:
        int index = 0
        TreeNode<String> treeNode = new TreeNode<>()
        TreeNode<String> childNodeAdded = new TreeNode<>("added child")
        treeNode.add(childNodeAdded)
        TreeNode<String> childNodeInserted = new TreeNode<>("inserted child")
        when:
        TreeNode<String> result = treeNode.insert(childNodeInserted, index)
        then:
        !treeNode.getChildren().isEmpty()
        treeNode.getChildCount() == 2
        treeNode.getChildAt(index).getData() == result.getData()
    }

    def "TreeNode isContainerAncestor returns true when check parent node"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>("parent")
        TreeNode<String> childNode = new TreeNode<>("child")
        treeNode.add(childNode)
        when:
        boolean result = childNode.isContainerAncestor(treeNode)
        then:
        result
    }

    def "TreeNode isContainerAncestor returns false when check child node"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>("parent")
        TreeNode<String> childNode = new TreeNode<>("child")
        treeNode.add(childNode)
        when:
        boolean result = treeNode.isContainerAncestor(childNode)
        then:
        !result
    }

    def "TreeNode isContainerAncestor returns false when check alien node"() {
        given:
        TreeNode<String> treeNode1 = new TreeNode<>("parent1")
        TreeNode<String> treeNode2 = new TreeNode<>("parent2")
        TreeNode<String> childNode = new TreeNode<>("child")
        treeNode2.add(childNode)
        when:
        boolean result = treeNode1.isContainerAncestor(childNode)
        then:
        !result
    }

    def "TreeNode remove child by value"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        TreeNode<String> childNode = new TreeNode<>("child")
        treeNode.add(childNode)
        when:
        boolean result = treeNode.remove(childNode)
        then:
        result
        !treeNode.hasChildren()
    }

    def "TreeNode remove throws exception if child is null"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        TreeNode<String> childNode = null
        when:
        boolean result = treeNode.remove(childNode)
        then:
        IllegalArgumentException ex = thrown()
        ex.message == 'Argument is null'
        !result
    }

    def "TreeNode remove throws exception for alien child"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        TreeNode<String> childNodeAlien = new TreeNode<>("alien child")
        treeNode.add(new TreeNode<>("child"))
        when:
        boolean result = treeNode.remove(childNodeAlien)
        then:
        IllegalArgumentException ex = thrown()
        ex.message == 'Argument is not a child'
        !result
    }

    def "TreeNode remove child by index"() {
        given:
        TreeNode<String> treeNode = new TreeNode<>()
        TreeNode<String> childNode = new TreeNode<>("child")
        treeNode.add(childNode)
        int index = treeNode.getIndex(childNode)
        when:
        boolean result = treeNode.remove(index)
        then:
        result
        !treeNode.hasChildren()
        childNode.getParent() == null
    }
}

